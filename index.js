// ===================SETUP THE DEPENDENCIES===================

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();// to hide password

const taskRoute = require("./routes/taskRoute");// calls the data under taskRoute.js



//==========SERVER SETUP =================

const app = express();
const port = 3002;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//==========DATABASE CONNECTION===============
//CONNECT TO MONGODB ATLAS

mongoose.connect(`mongodb+srv://mculaban6:${process.env.PASSWORD}@cluster0.ltc6n0r.mongodb.net/MRC?retryWrites=true&w=majority`, 
    {

        useNewUrlParser : true,
        useUnifiedTopology : true
    }
);

//==========SETUP NOTIFICATION FOR CONNECTION SUCCESS OR FAILURE=======
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.on('open', () => console.log("Connected to MongoDB"));

//===========ADDS THE TASK ROUTES=========
// Allows all the task routes created in the "taskRoute.js" file to use "/task" route


app.use("/tasks", taskRoute);// all endpoints


//==========SERVER LISTENING=================
app.listen(port, () => console.log(`Now listening to port ${port}`));




