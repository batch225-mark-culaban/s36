// CREATE THE SCHEMA, MODEL AND EXPORT THE FILE
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

    name: String,
 // age: Number,
    status: {
        type: String,
        default : "pending"
    } 
});

// MODULE.EXPORTS is the way for Node JS to treat this value as "package" that can be used by other files.

module.exports = mongoose.model("Task", taskSchema);//to export to other files or to transfer schema to other files(usable)

