// Controllers contain the functions and business logic of our Express JS. Meaning all the operation it can do will be placed in this file.

const Task = require("../models/task");//isulod ang task folder dri


//==============module for get ======================
module.exports.getAllTasks = () => {

    // The "return" statement. Returns the result of the Mongoose method
    // Then "then" method is used to wait for the Mongoose method to finish before sending the result back to the route.

    return Task.find({}).then(result => {//to see all database content

    return result;//return result in Postman
    });

}

//=====================module for creating task ====================
module.exports.createTask = (requestBody) => {
    // Create a task object based on the Mongoose model "Task"
    let newTask = new Task({
        
        name : requestBody.name
    })
    return newTask.save().then((task, error) => {
        
        if (error) {
            console.log(error);

            //if an error is encountered, the "return" statement will prevent any other line or code within the same code block

            //  The else statement will no longer evaluated 
            return false;
        } else {
            return task
        }


    })
}

// ================= module for delete ========================

module.exports.deleteTask = (taskId) => {
    // The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB
	// The Mongoose method "findByIdAndRemove" method looks for the document using the "_id" field
    
    return Task.findByIdAndRemove(taskId).then((result, err) => {

        if (err){
            console.log(err);
            return false;
        }else {
            return result
        }
    })

}



// =======module for put ==============

module.exports.updateTask = (taskId, newContent) => {

    return Task.findById(taskId).then((result, err) => {

        if(err){
            console.log(err);
            return false;
        } else {
              
            result.name = newContent.name;

             return result.save().then((updated, saveErr) => {
                         if (saveErr) {
                                         console.log(saveErr);
                                        return false;
                         } else {
                                        return updated;
                                 }
            })
        }
    })
}
//module.exports, to export to other file


//============ ACTIVITY ===============

module.exports.updateStatus = (taskId, newContent) => {

    return Task.findById(taskId).then((result, error) => {

        if (error) {
            console.log(error)
            return false;

        } else {
            result.status = newContent.status;

            return result.save().then((updated, saveError) => {

                if (saveError) {
                    console.log(saveError);
                    return false;
                } else {
                    return updated;
                }

            })
        }
    })
}


module.exports.getTaskbyId = (id) => {
    return Task.findById(id).then(result => {
        return result;
    })
}

