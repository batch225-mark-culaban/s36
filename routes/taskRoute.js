// Contains all the endpoints for our application

// We need to use express' Router() function to achieve this

const express = require("express");

// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application.
const router = express.Router();


const taskController = require("../controllers/taskController");//to link with taskController.js file//filename lng .,wala extension


/*for specific calling
    SYNTAX : localhost:3001/tasks/getinfo


*/


// ===========ROUTES=======//gets content under controller folder

// ============== Route to get all the task ==================
router.get("/getinfo", (req, res) => {

    // code functions
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
    //.send sends through Postman


});

//============ Route to create a new task, create router ==================
router.post("/create", (req, res) => {
        // If information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property

        taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
//resultFromController, pwede dn "result"
})

//========= ROUTE TO DELETE A TASK ================
// The colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
// The word that comes after the colon (:) symbol will be the name of the new URL parameter
// ":id" is a wildcard where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL.
/*

    example.
    localhost:3001/tasks/delete/1234(1234 ang unique id )
    (the 1234 is assigned to the "id" parameter in the URL)
*/

router.delete("/delete/:id", (req, res) => {
//id kay unique


    // URL parameter values are accessed via the request object's "params" property
	// The property name of this object will match the given URL parameter name
	// In this case "id" is the name of the parameter
	// If information will be coming from the URL, the data can be accessed from the request "params" property
    taskController.deleteTask(req.params.id).then(result => res.send(result));

});

//================ Route to update a task ====================
router.put("/update/:id", (req, res) => {

    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))

});



// Use "module.exports" to exports the router object to use in the "app.js"

module.exports = router;//to exports data inside taskRoute as imports data from taskRoute to index.js






// ============= ACTIVITY ===================






router.put("/updateStatus/:id/complete", (req, res) => {

    taskController.updateStatus(req.params.id ,req.body).then(result => res.send(result));
});



router.get("/:id", (req, res) => {
    const id = req.params.id;
    taskController.getTaskbyId(id).then(resultFromController => res.send(resultFromController));
});

module.exports = router;